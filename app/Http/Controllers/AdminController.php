<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
class AdminController extends Controller
{
      public function login(Request $request){
        if($request->isMethod('post')){
          $data = $request->input();
          if(Auth::attempt(['email'=>$data['username'],'password'=>$data['password']])){
              return redirect('admin/dashboard');
          }else{
              return redirect('/admin')->with('flash_message_error', 'Invalid username password');
          }
        }
        return view('admin.admin_login');
    }
    public function dashboard(){
        return view('admin.dashboard');
    }
    public function logout(){
        Session::flush(); //mean jitna session ho remove ho jaye ga
        return redirect ('/admin')->with('flash_message_error', 'You are loged out');
    }
}
