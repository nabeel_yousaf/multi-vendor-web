<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
 Route::match(['get', 'post'], '/', 'IndexController@index');
Route::match(['get', 'post'], '/admin', 'AdminController@login');
Route::match(['get', 'post'], '/vendor/dashboard', 'VendorController@dashboard');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>['auth']], function(){
Route::match(['get', 'post'], '/admin/dashboard', 'AdminController@dashboard');

});

Route::get('/logout', 'AdminController@logout');
